import React, {useEffect, useState} from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function App() {
  return (
    <Router>
      <div>
        <Route exact path="/" component={Risks} />
        <Route exact path="/risks" component={Risks} />
        <Route path="/machines" component={Machines} />
      </div>
    </Router>
  );
}

function Risks() {
  const [items, setItems] = useState([]);
  const [sortBy, setSortBy] = useState("");

  // I haven't got enough time to finish up the filter
  // const [filterBy, setFilterBy] = useState("");
  // const [filterValue, setFilterValue] = useState("");

  useEffect(() => { 

    const sortAndFilterItems = items => {
      /*
      if (filterValue) {
        switch (filterBy) {
          case "name":
            items = items.filter(item => item.description.includes(filterValue));
            break;
          case "date":
            items = items.filter(item => item.date > filterValue);
            break;
          case "riskLevel":
            items = items.filter(item => item.level === filterValue);
            break;
          default:
            break;
        }
      }
      */
  
      if (sortBy) {
        switch (sortBy) {
          case "name":
            items = items.sort((a, b) => a.description > b.description);
            break;
          case "date":
          items = items.sort((a, b) => a.date - b.date);
            break;
          case "riskLevel":
          items = items.sort((a, b) => a.level - b.level);
            break;
          default:
            break;
        }
      }
      return items;
    }

    const intervalID = setInterval(
      async function fetchData() {
        const response = await fetch("http://localhost:4000/risks");
        let json = await response.json();
        json = sortAndFilterItems(json);
        setItems(json);
      }, 1000);
    return () => clearInterval(intervalID);
  }, [sortBy]);

  

  const severity = level => {
    switch (level) {
      case 0:
        return "low";
      case 1:
        return "medium";
      case 2:
        return "high";
      default:
        return "unknown"
    }
  }

  const foundOnDate = foundOn => new Date(foundOn)
    .toISOString()
    .substring(0, 16)
    .replace("T", " ");

  return (
    <div>
      <Link to="machines">machines</Link><br/>
      risks:<br/>
      <button onClick={() => setSortBy("name")}>Sort by name</button>
      <button onClick={() => setSortBy("date")}>Sort by date</button>
      <button onClick={() => setSortBy("riskLevel")}>Sort by risk</button>
      <table><tbody>
        <tr>
          <th>Found on</th>
          <th>Risk</th>
          <th>Severity</th>
        </tr>
        {items.map(item => <tr key={item.description}><td>{foundOnDate(item.foundOn)}</td><td>{item.description}</td><td>{severity(item.level)}</td></tr> )}
      </tbody></table>
    </div>);
}

function Machines() {
  // I haven't got enough time to finish up the machines
  return (
    <div>
      <Link to="risks">risks</Link><br/>
      machines 
    </div>);
}

export default App;
